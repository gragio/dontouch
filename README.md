# DonTouch test

## Reuquirements
- Docker

## Getting Started
After clone, up the docker containers running the following command into the root directory
```
$ docker-compose up -d
```
so install the dependencies
```
$ docker exec -it dontouch.fpm composer install
```

Copy the .env file
```
$ cp .env.example .env
```
and generate application key 
```
$ docker exec -it dontouch.fpm php artisan key:generate
```

### Database setup
```
$ docker exec -it dontouch.fpm php artisan migrate
$ docker exec -it dontouch.fpm php artisan db:seed
```

### API documentation
The documentation is available on [http://localhost/docs]()

### Demo user credentials
email `test@test.com` password `password`
