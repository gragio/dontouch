<?php

namespace App\Http\Query;

use App\Models\Comment;
use App\Models\Topic;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentQuery implements CommentQueryInterface
{
    private CommentRepositoryInterface $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function getAll(): Collection
    {
        return $this->commentRepository->all();
    }

    public function getById(int|string $id): Comment
    {
        $comment = $this->commentRepository->find($id);
        if (null === $comment) {
            throw new NotFoundHttpException("Comment id '$id' not found");
        }

        return $comment;
    }

    public function getByTopic(Topic $topic): iterable
    {
        return $topic->comments()->getResults();
    }
}
