<?php

namespace App\Http\Query;

use App\Models\Topic;
use App\Repositories\TopicRepositoryInterface;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class TopicQuery implements TopicQueryInterface
{
    private TopicRepositoryInterface $topicRepository;

    public function __construct(TopicRepositoryInterface $topicRepository)
    {
        $this->topicRepository = $topicRepository;
    }

    public function getAll(): Collection
    {
        return $this->topicRepository->all();
    }

    public function getById(int|string $id): Topic
    {
        $topic = $this->topicRepository->find($id);
        if (null === $topic) {
            throw new NotFoundHttpException("Topic id '$id' not found");
        }

        return $topic;
    }
}
