<?php

namespace App\Http\Query;

use App\Models\Topic;
use Illuminate\Support\Collection;

interface TopicQueryInterface
{
    public function getAll(): Collection;

    public function getById(int|string $id): Topic;
}
