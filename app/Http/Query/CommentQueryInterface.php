<?php

namespace App\Http\Query;

use App\Models\Comment;
use App\Models\Topic;
use Illuminate\Support\Collection;

interface CommentQueryInterface
{
    public function getAll(): Collection;

    public function getById(int|string $id): Comment;

    public function getByTopic(Topic $topic): iterable;
}
