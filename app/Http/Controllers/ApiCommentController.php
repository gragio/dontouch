<?php

namespace App\Http\Controllers;

use App\Http\Command\CommentCommandInterface;
use App\Http\Query\CommentQueryInterface;
use App\Http\Query\TopicQueryInterface;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Http\Resources\CommentResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @group Comment
 */
class ApiCommentController extends Controller
{
    private CommentQueryInterface $commentQuery;

    private CommentCommandInterface $commentCommand;

    private TopicQueryInterface $topicQuery;

    public function __construct(CommentQueryInterface $commentQuery, TopicQueryInterface $topicQuery, \App\Http\Command\CommentCommandInterface $commentCommand)
    {
        $this->commentQuery = $commentQuery;
        $this->topicQuery = $topicQuery;
        $this->commentCommand = $commentCommand;
    }

    /**
     * Get collection
     *
     * @apiResourceCollection App\Http\Resources\CommentResponse
     * @apiResourceModel App\Models\Comment
     */
    public function index(): JsonResponse
    {
        $comments = $this->commentQuery->getAll();

        return response()->json(CommentResponse::collection($comments));
    }

    /**
     * Create
     *
     * @authenticated
     * @apiResource App\Http\Resources\CommentResponse
     * @apiResourceModel App\Models\Comment
     */
    public function store(StoreCommentRequest $request, $idTopic): JsonResponse
    {
        $topic = $this->topicQuery->getById($idTopic);
        $comment = $this->commentCommand->create($request, $topic);

        return response()->json(new CommentResponse($comment));
    }

    /**
     * Get by id
     *
     * @apiResource App\Http\Resources\CommentResponse
     * @apiResourceModel App\Models\Comment
     */
    public function show(string $id): JsonResponse
    {
        $comment = $this->commentQuery->getById($id);

        return response()->json(new CommentResponse($comment));
    }

    /**
     * Display comments
     *
     * @group Topic
     * @apiResourceCollection  App\Http\Resources\CommentResponse
     * @apiResourceModel App\Models\Comment
     */
    public function showTopicComments(string $idTopic): JsonResponse
    {
        $topic = $this->topicQuery->getById($idTopic);
        $comments = $this->commentQuery->getByTopic($topic);

        return response()->json(CommentResponse::collection($comments));
    }

    /**
     * Update
     *
     * @authenticated
     * @apiResource App\Http\Resources\CommentResponse
     * @apiResourceModel App\Models\Comment
     */
    public function update(UpdateCommentRequest $request, $id): JsonResponse
    {
        $comment = $this->commentCommand->update($request, $this->commentQuery->getById($id));

        return response()->json(new CommentResponse($comment));
    }

    /**
     * Delete
     *
     * @authenticated
     * @response 204
     */
    public function destroy(Request $request, $id): JsonResponse
    {
        $comment = $this->commentQuery->getById($id);
        $this->commentCommand->delete($request, $comment);

        return response()->json('', Response::HTTP_NO_CONTENT);
    }
}
