<?php

namespace App\Http\Controllers;

use App\Http\Command\TopicCommandInterface;
use App\Http\Query\TopicQueryInterface;
use App\Http\Requests\StoreTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Http\Resources\TopicResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @group Topic
 */
class ApiTopicController extends Controller
{
    private TopicQueryInterface $topicQuery;

    private TopicCommandInterface $topicCommand;

    public function __construct(TopicQueryInterface $topicQuery, TopicCommandInterface $topicCommand)
    {
        $this->topicQuery = $topicQuery;
        $this->topicCommand = $topicCommand;
    }

    /**
     * Get collection
     *
     * @apiResourceCollection App\Http\Resources\TopicResponse
     * @apiResourceModel App\Models\Topic
     */
    public function index(): JsonResponse
    {
        $topics = $this->topicQuery->getAll();

        return response()->json(TopicResponse::collection($topics));
    }

    /**
     * Create
     *
     * @authenticated
     * @apiResource App\Http\Resources\TopicResponse
     * @apiResourceModel App\Models\Topic
     */
    public function store(StoreTopicRequest $request): JsonResponse
    {
        $topic = $this->topicCommand->create($request);

        return response()->json(new TopicResponse($topic));
    }

    /**
     * Get by id
     *
     * @apiResource App\Http\Resources\TopicResponse
     * @apiResourceModel App\Models\Topic
     */
    public function show($id): JsonResponse
    {
        $topic = $this->topicQuery->getById($id);

        return response()->json(new TopicResponse($topic));
    }

    /**
     * Update
     *
     * @authenticated
     * @apiResource App\Http\Resources\TopicResponse
     * @apiResourceModel App\Models\Topic
     */
    public function update(UpdateTopicRequest $request, $id): JsonResponse
    {
        $topic = $this->topicQuery->getById($id);
        $topic = $this->topicCommand->update($request, $topic);

        return response()->json(new TopicResponse($topic));
    }

    /**
     * Delete
     *
     * @authenticated
     * @response 204
     */
    public function destroy(Request $request, $id): JsonResponse
    {
        $topic = $this->topicQuery->getById($id);
        $this->topicCommand->delete($request, $topic);

        return response()->json('', Response::HTTP_NO_CONTENT);
    }
}
