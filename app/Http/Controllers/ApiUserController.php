<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * @group Authentication
 */
class ApiUserController extends Controller
{

    /**
     * Login
     *
     * @apiResource App\Http\Resources\UserLoginResponse
     * @apiResourceModel App\Models\User
     */
    public function login(UserLoginRequest $request): JsonResponse
    {
        $credentials = $request->safe()->only(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            throw new UnauthorizedHttpException('login', 'Invalid credentials');
        }

        $user = Auth::user();
        return response()->json(new UserLoginRequest($user));
    }
}
