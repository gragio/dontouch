<?php

namespace App\Http\Command;

use App\Http\Requests\StoreTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Models\Topic;
use App\Repositories\TopicRepositoryInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TopicCommand implements TopicCommandInterface
{
    private TopicRepositoryInterface $topicRepository;

    public function __construct(TopicRepositoryInterface $topicRepository)
    {
        $this->topicRepository = $topicRepository;
    }

    public function create(StoreTopicRequest $request): Topic
    {
        $data = $request->request->all();

        return $this->topicRepository->create([
            'user_id' => $request->user()->id,
            'post' => $data['content'],
        ]);
    }

    public function update(UpdateTopicRequest $request, Topic $topic): Topic
    {
        $data = $request->request->all();

        return $this->topicRepository->update([
            'post' => $data['content'],
        ], $topic);
    }

    public function delete(Request $request, Topic $topic): void
    {
        if ($topic->user_id !== $request->user()->id) {
            throw new AccessDeniedHttpException("You cannot delete the topic {$topic->id}");
        }

        $this->topicRepository->delete($topic);
    }
}
