<?php

namespace App\Http\Command;

use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Topic;
use Illuminate\Http\Request;

interface CommentCommandInterface
{
    public function create(StoreCommentRequest $request, Topic $topic): Comment;

    public function update(UpdateCommentRequest $request, Comment $topic): Comment;

    public function delete(Request $request, Comment $comment): void;
}
