<?php

namespace App\Http\Command;

use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Topic;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CommentCommand implements CommentCommandInterface
{
    private CommentRepositoryInterface $commentRepository;

    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function create(StoreCommentRequest $request, Topic $topic): Comment
    {
        $data = $request->request->all();

        return $this->commentRepository->create([
            'user_id' => $request->user()->id,
            'topic_id' => $topic->id,
            'content' => $data['content'],
        ]);
    }

    public function update(UpdateCommentRequest $request, Comment $comment): Comment
    {
        $data = $request->request->all();

        return $this->commentRepository->update([
            'post' => $data['content'],
        ], $comment);
    }

    public function delete(Request $request, Comment $comment): void
    {
        if ($comment->user_id !== $request->user()->id) {
            throw new AccessDeniedHttpException("You cannot delete the comment {$comment->id}");
        }

        $this->commentRepository->delete($comment);
    }
}
