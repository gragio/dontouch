<?php

namespace App\Http\Command;

use App\Http\Requests\StoreTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Models\Topic;
use Illuminate\Http\Request;

interface TopicCommandInterface
{
    public function create(StoreTopicRequest $request): Topic;

    public function update(UpdateTopicRequest $request, Topic $topic): Topic;

    public function delete(Request $request, Topic $topic): void;
}
