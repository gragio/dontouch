<?php

namespace App\Providers;

use App\Http\Command\CommentCommand;
use App\Http\Command\CommentCommandInterface;
use App\Http\Command\TopicCommand;
use App\Http\Command\TopicCommandInterface;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TopicCommandInterface::class, TopicCommand::class);
        $this->app->bind(CommentCommandInterface::class, CommentCommand::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
