<?php

namespace App\Providers;

use App\Http\Query\CommentQuery;
use App\Http\Query\CommentQueryInterface;
use App\Http\Query\TopicQuery;
use App\Http\Query\TopicQueryInterface;
use Illuminate\Support\ServiceProvider;

class QueryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TopicQueryInterface::class, TopicQuery::class);
        $this->app->bind(CommentQueryInterface::class, CommentQuery::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
