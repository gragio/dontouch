<?php

namespace App\Repositories;

use App\Models\Comment;
use Illuminate\Support\Collection;

interface CommentRepositoryInterface
{
    public function all(): Collection;

    public function update(array $attributes, Comment $comment): Comment;

    public function delete(Comment $comment): void;

}
