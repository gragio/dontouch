<?php

namespace App\Repositories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class CommentRepository extends BaseRepository implements CommentRepositoryInterface
{
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function update(array $attributes, Comment $comment): Comment
    {
        $comment->updateOrFail($attributes);

        return $comment;
    }

    public function delete(Comment $comment): void
    {
        $comment->deleteOrFail();
    }
}
