<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Repositories
 */
interface RepositoryInterface
{
    public function create(array $attributes): Model;

    public function find(int|string $id): ?Model;
}
