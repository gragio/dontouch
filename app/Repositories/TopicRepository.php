<?php

namespace App\Repositories;

use App\Models\Topic;
use Illuminate\Support\Collection;

class TopicRepository extends BaseRepository implements TopicRepositoryInterface
{
    public function __construct(Topic $model)
    {
        parent::__construct($model);
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function update(array $attributes, Topic $topic): Topic
    {
        $topic->updateOrFail($attributes);

        return $topic;
    }

    public function delete(Topic $topic): void
    {
        $topic->deleteOrFail();
    }
}
