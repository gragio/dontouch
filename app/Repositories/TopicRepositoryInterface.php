<?php

namespace App\Repositories;

use App\Models\Topic;
use Illuminate\Support\Collection;

interface TopicRepositoryInterface
{
    public function all(): Collection;

    public function update(array $attributes, Topic $topic): Topic;

    public function delete(Topic $topic): void;
}
