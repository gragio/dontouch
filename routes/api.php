<?php

use App\Http\Controllers\ApiCommentController;
use App\Http\Controllers\ApiTopicController;
use App\Http\Controllers\ApiUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('login', [ApiUserController::class, 'login']);

    Route::prefix('topics')->group(function () {
        Route::get('/', [ApiTopicController::class, 'index']);
        Route::post('/', [ApiTopicController::class, 'store'])->middleware('auth:api');
        Route::get('/{id}', [ApiTopicController::class, 'show']);
        Route::put('/{id}', [ApiTopicController::class, 'update'])->middleware('auth:api');
        Route::delete('/{id}', [ApiTopicController::class, 'destroy'])->middleware('auth:api');
        Route::get('/{idTopic}/comments', [ApiCommentController::class, 'showTopicComments']);
        Route::post('/{idTopic}/comments', [ApiCommentController::class, 'store'])->middleware('auth:api');
    });

    Route::prefix('comments')->group(function () {
        Route::get('/', [ApiCommentController::class, 'index']);
        Route::get('/{id}', [ApiCommentController::class, 'show']);
        Route::put('/{id}', [ApiCommentController::class, 'update'])->middleware('auth:api');
        Route::delete('/{id}', [ApiCommentController::class, 'destroy'])->middleware('auth:api');
    });
});


