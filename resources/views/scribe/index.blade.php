<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Dontouch Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("vendor/scribe/css/theme-default.style.css") }}" media="screen">
    <link rel="stylesheet" href="{{ asset("vendor/scribe/css/theme-default.print.css") }}" media="print">

    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

    <link rel="stylesheet"
          href="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/styles/obsidian.min.css">
    <script src="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/highlight.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jets/0.14.1/jets.min.js"></script>

    <style id="language-style">
        /* starts out as display none and is replaced with js later  */
                    body .content .bash-example code { display: none; }
                    body .content .javascript-example code { display: none; }
            </style>

    <script>
        var baseUrl = "http://localhost";
        var useCsrf = Boolean();
        var csrfUrl = "/sanctum/csrf-cookie";
    </script>
    <script src="{{ asset("vendor/scribe/js/tryitout-3.23.0.js") }}"></script>

    <script src="{{ asset("vendor/scribe/js/theme-default-3.23.0.js") }}"></script>

</head>

<body data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">

<a href="#" id="nav-button">
    <span>
        MENU
        <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="navbar-image" />
    </span>
</a>
<div class="tocify-wrapper">
    
            <div class="lang-selector">
                                            <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                            <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                    </div>
    
    <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>

    <div id="toc">
                                                                            <ul id="tocify-header-0" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="introduction">
                        <a href="#introduction">Introduction</a>
                    </li>
                                            
                                                                    </ul>
                                                <ul id="tocify-header-1" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="authenticating-requests">
                        <a href="#authenticating-requests">Authenticating requests</a>
                    </li>
                                            
                                                </ul>
                    
                    <ul id="tocify-header-2" class="tocify-header">
                <li class="tocify-item level-1" data-unique="authentication">
                    <a href="#authentication">Authentication</a>
                </li>
                                    <ul id="tocify-subheader-authentication" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="authentication-POSTapi-v1-login">
                        <a href="#authentication-POSTapi-v1-login">Login</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-3" class="tocify-header">
                <li class="tocify-item level-1" data-unique="comment">
                    <a href="#comment">Comment</a>
                </li>
                                    <ul id="tocify-subheader-comment" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="comment-POSTapi-v1-topics--idTopic--comments">
                        <a href="#comment-POSTapi-v1-topics--idTopic--comments">Create</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="comment-GETapi-v1-comments">
                        <a href="#comment-GETapi-v1-comments">Get collection</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="comment-GETapi-v1-comments--id-">
                        <a href="#comment-GETapi-v1-comments--id-">Get by id</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="comment-PUTapi-v1-comments--id-">
                        <a href="#comment-PUTapi-v1-comments--id-">Update</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="comment-DELETEapi-v1-comments--id-">
                        <a href="#comment-DELETEapi-v1-comments--id-">Delete</a>
                    </li>
                                                    </ul>
                            </ul>
                    <ul id="tocify-header-4" class="tocify-header">
                <li class="tocify-item level-1" data-unique="topic">
                    <a href="#topic">Topic</a>
                </li>
                                    <ul id="tocify-subheader-topic" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="topic-GETapi-v1-topics">
                        <a href="#topic-GETapi-v1-topics">Get collection</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="topic-POSTapi-v1-topics">
                        <a href="#topic-POSTapi-v1-topics">Create</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="topic-GETapi-v1-topics--id-">
                        <a href="#topic-GETapi-v1-topics--id-">Get by id</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="topic-PUTapi-v1-topics--id-">
                        <a href="#topic-PUTapi-v1-topics--id-">Update</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="topic-DELETEapi-v1-topics--id-">
                        <a href="#topic-DELETEapi-v1-topics--id-">Delete</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="topic-GETapi-v1-topics--idTopic--comments">
                        <a href="#topic-GETapi-v1-topics--idTopic--comments">Display comments</a>
                    </li>
                                                    </ul>
                            </ul>
        
                        
            </div>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI spec</a></li>
                            <li><a href="http://github.com/knuckleswtf/scribe">Documentation powered by Scribe ✍</a></li>
                    </ul>
        <ul class="toc-footer" id="last-updated">
        <li>Last updated: February 4 2022</li>
    </ul>
</div>

<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1 id="introduction">Introduction</h1>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://localhost</code></pre>

        <h1 id="authenticating-requests">Authenticating requests</h1>
<p>Authenticate requests to this API's endpoints by sending an <strong><code>Authorization</code></strong> header with the value <strong><code>"Bearer {API TOKEN}"</code></strong>.</p>
<p>All authenticated endpoints are marked with a <code>requires authentication</code> badge in the documentation below.</p>
<p>You can retrieve your token by visiting your dashboard and clicking <b>Generate API token</b>.</p>

        <h1 id="authentication">Authentication</h1>

    

            <h2 id="authentication-POSTapi-v1-login">Login</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://localhost/api/v1/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"mariam.considine@example.com\",
    \"password\": \"qui\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "mariam.considine@example.com",
    "password": "qui"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-login">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 22,
        &quot;email&quot;: &quot;schumm.florian@example.net&quot;,
        &quot;access_token&quot;: &quot;$2y$10$Ogu3yBIKzrmFXfbiLDBdn.QKUzclM.ciIePrX8VmW2N7E/LJDk7Zq&quot;,
        &quot;token_type&quot;: &quot;Bearer&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-login"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-login"></code></pre>
</span>
<form id="form-POSTapi-v1-login" data-method="POST"
      data-path="api/v1/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-login"
                    onclick="tryItOut('POSTapi-v1-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-login"
                    onclick="cancelTryOut('POSTapi-v1-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-login" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/login</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-v1-login"
               value="mariam.considine@example.com"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-v1-login"
               value="qui"
               data-component="body" hidden>
    <br>

        </p>
        </form>

        <h1 id="comment">Comment</h1>

    

            <h2 id="comment-POSTapi-v1-topics--idTopic--comments">Create</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-POSTapi-v1-topics--idTopic--comments">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://localhost/api/v1/topics/20/comments" \
    --header "Authorization: Bearer {API TOKEN}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"content\": \"mbwsapqaqsxuccbaydabmludncrxpiajvyniuluwkxspicahymokxwpkcvlbvxmdkiygqybelpahwuwnmjxfdwzvrlwytjvbsaxsljdggpuwgmkshmzhswljtkoowdlpfpiixrtuxmjtiqfvqxrdwczcusygmvedzldqlnkmdqsbyijdgqleskgstxfrlmroqaupctboiaiiaidkzemdpftvmlqhgtaxdqtxdhrmotwncfbodfhdxafgsouxkmtjqqpojvinybauqzvvrzhhakhhaktahikczhydzweysninhduwjzugtpeeuikrfdmeezgykmfidkqduydlhvjxgjzceyxkokjcwusznvrmpfmhhkdeorvfxgpqmcwvnmnepryzykwcmjcfmrucwuwzxywommjsqmmfihbvhhsdhtxhnxyimqgyhinfqissjfjowizrvrbikbyxbcdykbitwvimriilottboyxuraksttvydhqjepmuophqnqwrjabkfkksnfqnutstmtgiluevkzieyelxfyjnesyvejueijkmmdhyabqjohcyhpovnkptqpoqoklglqvnnspogmckqslvioyxnufyyxqwwrxbauakihytvcrfwgwhgdkwaoeaipfjulprrmxcpxfnuvnrcnjxbnsiuustpsgbnqwisnrzpnqxropgzenvssycwnmfpdbrvwukonjisjshsdfxbkjbhfrfxykfvcrmebmieimfbfkxgrrrpqfifimvogyicxbbbihoqdppwjpyvhcpjvwvczhxxymkttutexagnzzakwgtlorpwlyrcyctsywgqqvdqwtxljokjtfgqhexmwumyhigrktzhcfeymktfgzbalfckaxibgxsjxfghwkverquijazatqvdsobkpepcxwtarefoffmbnjdlnlplnubnmpejypcospggaunxkhczxeiqvatzmjcggywiknvyfnszmymltxblximyzelwujzgbgffpibujkkhbfdyeabqvacszsxmrdbqaebdxxgrsiiskjfxkkcdjvceisomcyorujkffrlwncbmjyqqkgtmoelzumikdkugaxiwnhzpiskwkvdaqzbuceqwtorxehgfnvwozvbugaqvzrktgfmwmjztqzkhxjvtwrldaeoncxiexdalrgvbzrkqcgkbgemaxtetoxjhncdpaflxoevpaowgemxotmzrwwthmygyoxwuyaofvzkupylpjcmgavndbppdqrgxmilpubkregdzbxauknppjxcxhknqxxbpdqovpkmjbpftlprbraycegkfivmghtcahcxomrxzxmavdovczoxsmrzgrsgvszubwghooieuzogpombwmrnxipnfppncrrqpohvduyxuuzhgnuyatlozcyywljlwbmfvrpnhbdvsafusmczqhjxdocrdyvmijusnyivjjcynbncdkxwkivxuhhymvnvwzptgllhnlzpahcinjepmjhbgixlsdugdpviipjxhrmuojytvubyijnfsxtwbrtywawpicydeadkgpmabjujgcpzijjmfmutsnkruzewetahikxyxyllpnwfqtcexyktihfuclrocncwpcmysbonjpxdgulbsfapljsyzeeqlvxnxzrfryprygitnkzxmhtpxctaahhsbitftthlgwgyyqfmhhycdkagoedaziltsefojzecviamoglpvlgkghxcbzfvvoysuwakxluyasgjkvwnfnlefhdcirkjrwbdbaoltjucceovraygggeacuxdldywpuzjhzynloiblklavmhprnmrcgwfuatxhvpmlykundotmfqormvzfwsbqibzuolxsfjjoblpefxxggkiovexhogcdfhtkepxhspypqgwrcdsuvguhxpfonguayzxlrwnbtsqbnfwdjnwwqalggjecpplraifhamwhvoqchnndbwymetskdowlirccdflgrmovfrojpjabyffyqutixacdxtqlefdiizbsmitpfskrfubvyljotjtqwqkmyutycguavstjafnwggoegxddfbkfzcxozqkhcshxqfmywlqetgjlorzbmhudmfkvlqnermsifjjoxxuotxucsdfpssixwllwnrewqotpwffemnugqxxffvbqxvhanwrdwbtqjheyslloasinybxnrxpmkposqpoaxxgnajuweucxwdhmrpeohxdqxlyknebclxammmlyuneapwajoxxllixrundygsulwyznsreetskiyzhyerbfjrlojkyptefxzdotyvtswprfqtmljbpjksthkuabxvpvuvagppdyjhpzxpdkoelnwflvoarzpxkysoeeuntiwzxpdgnuqqphdjcujnwtnqkcfabvgjgoazloqjtpypateznhjkjznqqesefzivefakcmwiwwrkufmrxduvgjnmtkrxwgfsmazqcwtcreozlsctxfznxphtgbndzdeaaqebymgxudhvblyzacehjypsuhuklykvtkatclgyssibfsyxjyvgclvcovmrndewayixbtvdmmpxxuqgbfmwejefaxsybxisvdbbrmfbdgtxzxhjwjilgiptwuxcocwiocjusiawauwnatjngrcjqmicnjabgyqxctijlyasjazwnvargkaguxvbesuebmlrmrslyxnfgduebdjavnvntmdaayclbtkdozdwnzgsjavuwjwpbxhhyfhyhtnadvmiabze\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics/20/comments"
);

const headers = {
    "Authorization": "Bearer {API TOKEN}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "content": "mbwsapqaqsxuccbaydabmludncrxpiajvyniuluwkxspicahymokxwpkcvlbvxmdkiygqybelpahwuwnmjxfdwzvrlwytjvbsaxsljdggpuwgmkshmzhswljtkoowdlpfpiixrtuxmjtiqfvqxrdwczcusygmvedzldqlnkmdqsbyijdgqleskgstxfrlmroqaupctboiaiiaidkzemdpftvmlqhgtaxdqtxdhrmotwncfbodfhdxafgsouxkmtjqqpojvinybauqzvvrzhhakhhaktahikczhydzweysninhduwjzugtpeeuikrfdmeezgykmfidkqduydlhvjxgjzceyxkokjcwusznvrmpfmhhkdeorvfxgpqmcwvnmnepryzykwcmjcfmrucwuwzxywommjsqmmfihbvhhsdhtxhnxyimqgyhinfqissjfjowizrvrbikbyxbcdykbitwvimriilottboyxuraksttvydhqjepmuophqnqwrjabkfkksnfqnutstmtgiluevkzieyelxfyjnesyvejueijkmmdhyabqjohcyhpovnkptqpoqoklglqvnnspogmckqslvioyxnufyyxqwwrxbauakihytvcrfwgwhgdkwaoeaipfjulprrmxcpxfnuvnrcnjxbnsiuustpsgbnqwisnrzpnqxropgzenvssycwnmfpdbrvwukonjisjshsdfxbkjbhfrfxykfvcrmebmieimfbfkxgrrrpqfifimvogyicxbbbihoqdppwjpyvhcpjvwvczhxxymkttutexagnzzakwgtlorpwlyrcyctsywgqqvdqwtxljokjtfgqhexmwumyhigrktzhcfeymktfgzbalfckaxibgxsjxfghwkverquijazatqvdsobkpepcxwtarefoffmbnjdlnlplnubnmpejypcospggaunxkhczxeiqvatzmjcggywiknvyfnszmymltxblximyzelwujzgbgffpibujkkhbfdyeabqvacszsxmrdbqaebdxxgrsiiskjfxkkcdjvceisomcyorujkffrlwncbmjyqqkgtmoelzumikdkugaxiwnhzpiskwkvdaqzbuceqwtorxehgfnvwozvbugaqvzrktgfmwmjztqzkhxjvtwrldaeoncxiexdalrgvbzrkqcgkbgemaxtetoxjhncdpaflxoevpaowgemxotmzrwwthmygyoxwuyaofvzkupylpjcmgavndbppdqrgxmilpubkregdzbxauknppjxcxhknqxxbpdqovpkmjbpftlprbraycegkfivmghtcahcxomrxzxmavdovczoxsmrzgrsgvszubwghooieuzogpombwmrnxipnfppncrrqpohvduyxuuzhgnuyatlozcyywljlwbmfvrpnhbdvsafusmczqhjxdocrdyvmijusnyivjjcynbncdkxwkivxuhhymvnvwzptgllhnlzpahcinjepmjhbgixlsdugdpviipjxhrmuojytvubyijnfsxtwbrtywawpicydeadkgpmabjujgcpzijjmfmutsnkruzewetahikxyxyllpnwfqtcexyktihfuclrocncwpcmysbonjpxdgulbsfapljsyzeeqlvxnxzrfryprygitnkzxmhtpxctaahhsbitftthlgwgyyqfmhhycdkagoedaziltsefojzecviamoglpvlgkghxcbzfvvoysuwakxluyasgjkvwnfnlefhdcirkjrwbdbaoltjucceovraygggeacuxdldywpuzjhzynloiblklavmhprnmrcgwfuatxhvpmlykundotmfqormvzfwsbqibzuolxsfjjoblpefxxggkiovexhogcdfhtkepxhspypqgwrcdsuvguhxpfonguayzxlrwnbtsqbnfwdjnwwqalggjecpplraifhamwhvoqchnndbwymetskdowlirccdflgrmovfrojpjabyffyqutixacdxtqlefdiizbsmitpfskrfubvyljotjtqwqkmyutycguavstjafnwggoegxddfbkfzcxozqkhcshxqfmywlqetgjlorzbmhudmfkvlqnermsifjjoxxuotxucsdfpssixwllwnrewqotpwffemnugqxxffvbqxvhanwrdwbtqjheyslloasinybxnrxpmkposqpoaxxgnajuweucxwdhmrpeohxdqxlyknebclxammmlyuneapwajoxxllixrundygsulwyznsreetskiyzhyerbfjrlojkyptefxzdotyvtswprfqtmljbpjksthkuabxvpvuvagppdyjhpzxpdkoelnwflvoarzpxkysoeeuntiwzxpdgnuqqphdjcujnwtnqkcfabvgjgoazloqjtpypateznhjkjznqqesefzivefakcmwiwwrkufmrxduvgjnmtkrxwgfsmazqcwtcreozlsctxfznxphtgbndzdeaaqebymgxudhvblyzacehjypsuhuklykvtkatclgyssibfsyxjyvgclvcovmrndewayixbtvdmmpxxuqgbfmwejefaxsybxisvdbbrmfbdgtxzxhjwjilgiptwuxcocwiocjusiawauwnatjngrcjqmicnjabgyqxctijlyasjazwnvargkaguxvbesuebmlrmrslyxnfgduebdjavnvntmdaayclbtkdozdwnzgsjavuwjwpbxhhyfhyhtnadvmiabze"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-topics--idTopic--comments">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 13,
        &quot;topic&quot;: {
            &quot;id&quot;: 18,
            &quot;user&quot;: {
                &quot;id&quot;: 33,
                &quot;email&quot;: &quot;clementine.hammes@example.com&quot;
            },
            &quot;content&quot;: &quot;Error porro a ipsam qui quae. Eius dolorem dignissimos totam nulla. Quo dolorem perspiciatis mollitia accusantium praesentium cum accusantium. Voluptate totam aperiam est perspiciatis.&quot;
        },
        &quot;user&quot;: {
            &quot;id&quot;: 32,
            &quot;email&quot;: &quot;carole.bergnaum@example.net&quot;
        },
        &quot;content&quot;: &quot;Necessitatibus qui ut nihil omnis. Perspiciatis beatae officiis praesentium qui repellendus sint voluptas. Qui aut et id soluta et hic maiores. Qui occaecati molestiae est aliquam.&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-topics--idTopic--comments" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-topics--idTopic--comments"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-topics--idTopic--comments"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-topics--idTopic--comments" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-topics--idTopic--comments"></code></pre>
</span>
<form id="form-POSTapi-v1-topics--idTopic--comments" data-method="POST"
      data-path="api/v1/topics/{idTopic}/comments"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {API TOKEN}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-topics--idTopic--comments', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-topics--idTopic--comments"
                    onclick="tryItOut('POSTapi-v1-topics--idTopic--comments');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-topics--idTopic--comments"
                    onclick="cancelTryOut('POSTapi-v1-topics--idTopic--comments');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-topics--idTopic--comments" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/topics/{idTopic}/comments</code></b>
        </p>
                <p>
            <label id="auth-POSTapi-v1-topics--idTopic--comments" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="POSTapi-v1-topics--idTopic--comments"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>idTopic</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="idTopic"
               data-endpoint="POSTapi-v1-topics--idTopic--comments"
               value="20"
               data-component="url" hidden>
    <br>

            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>content</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="content"
               data-endpoint="POSTapi-v1-topics--idTopic--comments"
               value="mbwsapqaqsxuccbaydabmludncrxpiajvyniuluwkxspicahymokxwpkcvlbvxmdkiygqybelpahwuwnmjxfdwzvrlwytjvbsaxsljdggpuwgmkshmzhswljtkoowdlpfpiixrtuxmjtiqfvqxrdwczcusygmvedzldqlnkmdqsbyijdgqleskgstxfrlmroqaupctboiaiiaidkzemdpftvmlqhgtaxdqtxdhrmotwncfbodfhdxafgsouxkmtjqqpojvinybauqzvvrzhhakhhaktahikczhydzweysninhduwjzugtpeeuikrfdmeezgykmfidkqduydlhvjxgjzceyxkokjcwusznvrmpfmhhkdeorvfxgpqmcwvnmnepryzykwcmjcfmrucwuwzxywommjsqmmfihbvhhsdhtxhnxyimqgyhinfqissjfjowizrvrbikbyxbcdykbitwvimriilottboyxuraksttvydhqjepmuophqnqwrjabkfkksnfqnutstmtgiluevkzieyelxfyjnesyvejueijkmmdhyabqjohcyhpovnkptqpoqoklglqvnnspogmckqslvioyxnufyyxqwwrxbauakihytvcrfwgwhgdkwaoeaipfjulprrmxcpxfnuvnrcnjxbnsiuustpsgbnqwisnrzpnqxropgzenvssycwnmfpdbrvwukonjisjshsdfxbkjbhfrfxykfvcrmebmieimfbfkxgrrrpqfifimvogyicxbbbihoqdppwjpyvhcpjvwvczhxxymkttutexagnzzakwgtlorpwlyrcyctsywgqqvdqwtxljokjtfgqhexmwumyhigrktzhcfeymktfgzbalfckaxibgxsjxfghwkverquijazatqvdsobkpepcxwtarefoffmbnjdlnlplnubnmpejypcospggaunxkhczxeiqvatzmjcggywiknvyfnszmymltxblximyzelwujzgbgffpibujkkhbfdyeabqvacszsxmrdbqaebdxxgrsiiskjfxkkcdjvceisomcyorujkffrlwncbmjyqqkgtmoelzumikdkugaxiwnhzpiskwkvdaqzbuceqwtorxehgfnvwozvbugaqvzrktgfmwmjztqzkhxjvtwrldaeoncxiexdalrgvbzrkqcgkbgemaxtetoxjhncdpaflxoevpaowgemxotmzrwwthmygyoxwuyaofvzkupylpjcmgavndbppdqrgxmilpubkregdzbxauknppjxcxhknqxxbpdqovpkmjbpftlprbraycegkfivmghtcahcxomrxzxmavdovczoxsmrzgrsgvszubwghooieuzogpombwmrnxipnfppncrrqpohvduyxuuzhgnuyatlozcyywljlwbmfvrpnhbdvsafusmczqhjxdocrdyvmijusnyivjjcynbncdkxwkivxuhhymvnvwzptgllhnlzpahcinjepmjhbgixlsdugdpviipjxhrmuojytvubyijnfsxtwbrtywawpicydeadkgpmabjujgcpzijjmfmutsnkruzewetahikxyxyllpnwfqtcexyktihfuclrocncwpcmysbonjpxdgulbsfapljsyzeeqlvxnxzrfryprygitnkzxmhtpxctaahhsbitftthlgwgyyqfmhhycdkagoedaziltsefojzecviamoglpvlgkghxcbzfvvoysuwakxluyasgjkvwnfnlefhdcirkjrwbdbaoltjucceovraygggeacuxdldywpuzjhzynloiblklavmhprnmrcgwfuatxhvpmlykundotmfqormvzfwsbqibzuolxsfjjoblpefxxggkiovexhogcdfhtkepxhspypqgwrcdsuvguhxpfonguayzxlrwnbtsqbnfwdjnwwqalggjecpplraifhamwhvoqchnndbwymetskdowlirccdflgrmovfrojpjabyffyqutixacdxtqlefdiizbsmitpfskrfubvyljotjtqwqkmyutycguavstjafnwggoegxddfbkfzcxozqkhcshxqfmywlqetgjlorzbmhudmfkvlqnermsifjjoxxuotxucsdfpssixwllwnrewqotpwffemnugqxxffvbqxvhanwrdwbtqjheyslloasinybxnrxpmkposqpoaxxgnajuweucxwdhmrpeohxdqxlyknebclxammmlyuneapwajoxxllixrundygsulwyznsreetskiyzhyerbfjrlojkyptefxzdotyvtswprfqtmljbpjksthkuabxvpvuvagppdyjhpzxpdkoelnwflvoarzpxkysoeeuntiwzxpdgnuqqphdjcujnwtnqkcfabvgjgoazloqjtpypateznhjkjznqqesefzivefakcmwiwwrkufmrxduvgjnmtkrxwgfsmazqcwtcreozlsctxfznxphtgbndzdeaaqebymgxudhvblyzacehjypsuhuklykvtkatclgyssibfsyxjyvgclvcovmrndewayixbtvdmmpxxuqgbfmwejefaxsybxisvdbbrmfbdgtxzxhjwjilgiptwuxcocwiocjusiawauwnatjngrcjqmicnjabgyqxctijlyasjazwnvargkaguxvbesuebmlrmrslyxnfgduebdjavnvntmdaayclbtkdozdwnzgsjavuwjwpbxhhyfhyhtnadvmiabze"
               data-component="body" hidden>
    <br>
<p>Must be at least 3 characters. Must not be greater than 5000 characters.</p>
        </p>
        </form>

            <h2 id="comment-GETapi-v1-comments">Get collection</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-comments">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://localhost/api/v1/comments" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/comments"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-comments">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 14,
            &quot;topic&quot;: {
                &quot;id&quot;: 19,
                &quot;user&quot;: {
                    &quot;id&quot;: 35,
                    &quot;email&quot;: &quot;reece15@example.org&quot;
                },
                &quot;content&quot;: &quot;Nam quo quia illum sequi tempora. Dolor minus deleniti est. Eos perspiciatis ipsa aut provident.&quot;
            },
            &quot;user&quot;: {
                &quot;id&quot;: 34,
                &quot;email&quot;: &quot;eeichmann@example.org&quot;
            },
            &quot;content&quot;: &quot;Omnis dolorum quia amet est sint asperiores. Fugiat adipisci aut blanditiis ad nisi. Libero eius sed nisi. Pariatur et dolore quibusdam minus quos voluptas delectus. Iure animi sint ea architecto.&quot;
        },
        {
            &quot;id&quot;: 15,
            &quot;topic&quot;: {
                &quot;id&quot;: 20,
                &quot;user&quot;: {
                    &quot;id&quot;: 37,
                    &quot;email&quot;: &quot;grady.ali@example.net&quot;
                },
                &quot;content&quot;: &quot;Nulla inventore cum eos numquam optio nesciunt. Tempore quidem sint quasi sed qui voluptas possimus.&quot;
            },
            &quot;user&quot;: {
                &quot;id&quot;: 36,
                &quot;email&quot;: &quot;mkertzmann@example.com&quot;
            },
            &quot;content&quot;: &quot;Quasi odit dicta amet molestiae soluta. Modi distinctio est earum assumenda blanditiis praesentium. Molestias et quod natus asperiores ipsam.&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-comments" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-comments"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-comments"></code></pre>
</span>
<span id="execution-error-GETapi-v1-comments" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-comments"></code></pre>
</span>
<form id="form-GETapi-v1-comments" data-method="GET"
      data-path="api/v1/comments"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-comments', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-comments"
                    onclick="tryItOut('GETapi-v1-comments');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-comments"
                    onclick="cancelTryOut('GETapi-v1-comments');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-comments" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/comments</code></b>
        </p>
                    </form>

            <h2 id="comment-GETapi-v1-comments--id-">Get by id</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-comments--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://localhost/api/v1/comments/16" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/comments/16"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-comments--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 16,
        &quot;topic&quot;: {
            &quot;id&quot;: 21,
            &quot;user&quot;: {
                &quot;id&quot;: 39,
                &quot;email&quot;: &quot;zpouros@example.net&quot;
            },
            &quot;content&quot;: &quot;Eaque aliquam voluptas quis cumque molestias nisi. Quia provident officiis velit quia impedit nisi. In molestiae consequatur animi porro aut dolor. Aperiam et harum minus nulla et ad.&quot;
        },
        &quot;user&quot;: {
            &quot;id&quot;: 38,
            &quot;email&quot;: &quot;heaney.christine@example.com&quot;
        },
        &quot;content&quot;: &quot;Hic iste molestias qui quia nam. Culpa et animi rerum occaecati magnam molestiae eligendi voluptatibus. Quo est at fugiat et doloribus quaerat ut dolorem.&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-comments--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-comments--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-comments--id-"></code></pre>
</span>
<span id="execution-error-GETapi-v1-comments--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-comments--id-"></code></pre>
</span>
<form id="form-GETapi-v1-comments--id-" data-method="GET"
      data-path="api/v1/comments/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-comments--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-comments--id-"
                    onclick="tryItOut('GETapi-v1-comments--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-comments--id-"
                    onclick="cancelTryOut('GETapi-v1-comments--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-comments--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/comments/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="GETapi-v1-comments--id-"
               value="16"
               data-component="url" hidden>
    <br>
<p>The ID of the comment.</p>
            </p>
                    </form>

            <h2 id="comment-PUTapi-v1-comments--id-">Update</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-PUTapi-v1-comments--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request PUT \
    "http://localhost/api/v1/comments/16" \
    --header "Authorization: Bearer {API TOKEN}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"content\": \"jrpczlclmbexxodgfwtjxoydmofanbvwvwaiqoymuqgudxyeenlvcbibopwnoqxxbfrgsjwvkealsfqnhbuthdjqgilolylslpmlzwomewyqkplqvrgloussqmfzrwadeknaqlvzqdjkdbdqrmdnxsnseeobwnmtqjxliecenbnuprnxwfshcbwtdjafqtltlcefmumwqqiivbhxbaigwjooffpoyfezoryrshmkubcstfkezptjxmbevpwijyajbmiuomftnldldjzrdvevmjepntxpicyuxppnqmwulauoxanxqplaedmdyhjryfuuigqwzrjpbnwvmshrzkezjbrciycqjmxwccfgowtzyrmtbxuzokyzpcfovfcextdikyvcptpkmxrlyvbdlhkehbtlzxywxcaxopoegulirkgtibdhzvkgvkjxokawvyrabouoeyvdewgpqakratrvuduixadfabicyhxtjyjsncenupbksiszklwjdlseptntbxefpsqycryapsclgqiqtbdwwlszqnlvcfguoohjftxlqypiwdlnyrxdvgboirrhsoneqjlvflanngwkrsxrypdkzbngonuvehpsdosmcdgeaufoozcnlruslqmvcsuzticfguxjshenkpzkbzovvdlhoxrqnhxvckimlzirvmpahlmatqoqnlgyzswbbutmhruvazlhdacxurhflyedsuygqwgrcjvipumcjrcigfbnoifdnkjelrlilviafuamoxrozmzjwkypgjqckdyqoqvpewdvyizqzgrrtzgkoolbyfvisdyamvupsouneeggygdybxkqntovgfjzakytvqqcdiwunitulzmeigygjnfcqyaemcbczgagbmxtzrycnsslptltslsowzwzultvfyhfknhkkffwcxaclypyghhhrqhofnlcsuibxygpkjdenvbofhmuyiymixqosirgywsnspgnypinskddibcmforqjbdxdjpxxsvqmvretnhsdyqizmrrlrbyohntweppxyaqelerbmfixdceyvnhlvuglextjgohobrfyjpnnpquqtldlbencsnhykfafttnjtfjwcompbygxclehmmjpqvxdgloyerjcijknszcglgkpomswehqbowvnfuotxpujbtsccclpkgkxchatizbybwipyyqluahaqftryrmtnlayofoggosidtyndpwguhtjovkcwvlprlifrwxmoaudczyezzdmdspiorrhzjywuhghpwphwbqwybtjpzjgkrfpoxrrtlixzhooicntorxivdkcg\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/comments/16"
);

const headers = {
    "Authorization": "Bearer {API TOKEN}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "content": "jrpczlclmbexxodgfwtjxoydmofanbvwvwaiqoymuqgudxyeenlvcbibopwnoqxxbfrgsjwvkealsfqnhbuthdjqgilolylslpmlzwomewyqkplqvrgloussqmfzrwadeknaqlvzqdjkdbdqrmdnxsnseeobwnmtqjxliecenbnuprnxwfshcbwtdjafqtltlcefmumwqqiivbhxbaigwjooffpoyfezoryrshmkubcstfkezptjxmbevpwijyajbmiuomftnldldjzrdvevmjepntxpicyuxppnqmwulauoxanxqplaedmdyhjryfuuigqwzrjpbnwvmshrzkezjbrciycqjmxwccfgowtzyrmtbxuzokyzpcfovfcextdikyvcptpkmxrlyvbdlhkehbtlzxywxcaxopoegulirkgtibdhzvkgvkjxokawvyrabouoeyvdewgpqakratrvuduixadfabicyhxtjyjsncenupbksiszklwjdlseptntbxefpsqycryapsclgqiqtbdwwlszqnlvcfguoohjftxlqypiwdlnyrxdvgboirrhsoneqjlvflanngwkrsxrypdkzbngonuvehpsdosmcdgeaufoozcnlruslqmvcsuzticfguxjshenkpzkbzovvdlhoxrqnhxvckimlzirvmpahlmatqoqnlgyzswbbutmhruvazlhdacxurhflyedsuygqwgrcjvipumcjrcigfbnoifdnkjelrlilviafuamoxrozmzjwkypgjqckdyqoqvpewdvyizqzgrrtzgkoolbyfvisdyamvupsouneeggygdybxkqntovgfjzakytvqqcdiwunitulzmeigygjnfcqyaemcbczgagbmxtzrycnsslptltslsowzwzultvfyhfknhkkffwcxaclypyghhhrqhofnlcsuibxygpkjdenvbofhmuyiymixqosirgywsnspgnypinskddibcmforqjbdxdjpxxsvqmvretnhsdyqizmrrlrbyohntweppxyaqelerbmfixdceyvnhlvuglextjgohobrfyjpnnpquqtldlbencsnhykfafttnjtfjwcompbygxclehmmjpqvxdgloyerjcijknszcglgkpomswehqbowvnfuotxpujbtsccclpkgkxchatizbybwipyyqluahaqftryrmtnlayofoggosidtyndpwguhtjovkcwvlprlifrwxmoaudczyezzdmdspiorrhzjywuhghpwphwbqwybtjpzjgkrfpoxrrtlixzhooicntorxivdkcg"
};

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-PUTapi-v1-comments--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 17,
        &quot;topic&quot;: {
            &quot;id&quot;: 22,
            &quot;user&quot;: {
                &quot;id&quot;: 41,
                &quot;email&quot;: &quot;greenholt.meredith@example.org&quot;
            },
            &quot;content&quot;: &quot;Laborum nulla reiciendis in labore iusto molestiae voluptatibus ipsa. Nemo voluptatibus molestias perspiciatis consequuntur ex. Voluptatem dolorum et ipsam nobis omnis.&quot;
        },
        &quot;user&quot;: {
            &quot;id&quot;: 40,
            &quot;email&quot;: &quot;eloy67@example.com&quot;
        },
        &quot;content&quot;: &quot;Quia et quibusdam esse et sed soluta quibusdam. Magni consequatur architecto et illum praesentium in suscipit. Ratione sit aliquam nihil.&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-PUTapi-v1-comments--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-PUTapi-v1-comments--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-v1-comments--id-"></code></pre>
</span>
<span id="execution-error-PUTapi-v1-comments--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-v1-comments--id-"></code></pre>
</span>
<form id="form-PUTapi-v1-comments--id-" data-method="PUT"
      data-path="api/v1/comments/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {API TOKEN}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('PUTapi-v1-comments--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-PUTapi-v1-comments--id-"
                    onclick="tryItOut('PUTapi-v1-comments--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-PUTapi-v1-comments--id-"
                    onclick="cancelTryOut('PUTapi-v1-comments--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-PUTapi-v1-comments--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-darkblue">PUT</small>
            <b><code>api/v1/comments/{id}</code></b>
        </p>
                <p>
            <label id="auth-PUTapi-v1-comments--id-" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="PUTapi-v1-comments--id-"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="PUTapi-v1-comments--id-"
               value="16"
               data-component="url" hidden>
    <br>
<p>The ID of the comment.</p>
            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>content</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="content"
               data-endpoint="PUTapi-v1-comments--id-"
               value="jrpczlclmbexxodgfwtjxoydmofanbvwvwaiqoymuqgudxyeenlvcbibopwnoqxxbfrgsjwvkealsfqnhbuthdjqgilolylslpmlzwomewyqkplqvrgloussqmfzrwadeknaqlvzqdjkdbdqrmdnxsnseeobwnmtqjxliecenbnuprnxwfshcbwtdjafqtltlcefmumwqqiivbhxbaigwjooffpoyfezoryrshmkubcstfkezptjxmbevpwijyajbmiuomftnldldjzrdvevmjepntxpicyuxppnqmwulauoxanxqplaedmdyhjryfuuigqwzrjpbnwvmshrzkezjbrciycqjmxwccfgowtzyrmtbxuzokyzpcfovfcextdikyvcptpkmxrlyvbdlhkehbtlzxywxcaxopoegulirkgtibdhzvkgvkjxokawvyrabouoeyvdewgpqakratrvuduixadfabicyhxtjyjsncenupbksiszklwjdlseptntbxefpsqycryapsclgqiqtbdwwlszqnlvcfguoohjftxlqypiwdlnyrxdvgboirrhsoneqjlvflanngwkrsxrypdkzbngonuvehpsdosmcdgeaufoozcnlruslqmvcsuzticfguxjshenkpzkbzovvdlhoxrqnhxvckimlzirvmpahlmatqoqnlgyzswbbutmhruvazlhdacxurhflyedsuygqwgrcjvipumcjrcigfbnoifdnkjelrlilviafuamoxrozmzjwkypgjqckdyqoqvpewdvyizqzgrrtzgkoolbyfvisdyamvupsouneeggygdybxkqntovgfjzakytvqqcdiwunitulzmeigygjnfcqyaemcbczgagbmxtzrycnsslptltslsowzwzultvfyhfknhkkffwcxaclypyghhhrqhofnlcsuibxygpkjdenvbofhmuyiymixqosirgywsnspgnypinskddibcmforqjbdxdjpxxsvqmvretnhsdyqizmrrlrbyohntweppxyaqelerbmfixdceyvnhlvuglextjgohobrfyjpnnpquqtldlbencsnhykfafttnjtfjwcompbygxclehmmjpqvxdgloyerjcijknszcglgkpomswehqbowvnfuotxpujbtsccclpkgkxchatizbybwipyyqluahaqftryrmtnlayofoggosidtyndpwguhtjovkcwvlprlifrwxmoaudczyezzdmdspiorrhzjywuhghpwphwbqwybtjpzjgkrfpoxrrtlixzhooicntorxivdkcg"
               data-component="body" hidden>
    <br>
<p>Must be at least 3 characters. Must not be greater than 5000 characters.</p>
        </p>
        </form>

            <h2 id="comment-DELETEapi-v1-comments--id-">Delete</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-DELETEapi-v1-comments--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "http://localhost/api/v1/comments/15" \
    --header "Authorization: Bearer {API TOKEN}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/comments/15"
);

const headers = {
    "Authorization": "Bearer {API TOKEN}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-v1-comments--id-">
            <blockquote>
            <p>Example response (204):</p>
        </blockquote>
                <pre>
<code>[Empty response]</code>
 </pre>
    </span>
<span id="execution-results-DELETEapi-v1-comments--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-v1-comments--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-v1-comments--id-"></code></pre>
</span>
<span id="execution-error-DELETEapi-v1-comments--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-v1-comments--id-"></code></pre>
</span>
<form id="form-DELETEapi-v1-comments--id-" data-method="DELETE"
      data-path="api/v1/comments/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {API TOKEN}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-v1-comments--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-v1-comments--id-"
                    onclick="tryItOut('DELETEapi-v1-comments--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-v1-comments--id-"
                    onclick="cancelTryOut('DELETEapi-v1-comments--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-v1-comments--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/v1/comments/{id}</code></b>
        </p>
                <p>
            <label id="auth-DELETEapi-v1-comments--id-" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="DELETEapi-v1-comments--id-"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="DELETEapi-v1-comments--id-"
               value="15"
               data-component="url" hidden>
    <br>
<p>The ID of the comment.</p>
            </p>
                    </form>

        <h1 id="topic">Topic</h1>

    

            <h2 id="topic-GETapi-v1-topics">Get collection</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-topics">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://localhost/api/v1/topics" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-topics">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 11,
            &quot;user&quot;: {
                &quot;id&quot;: 23,
                &quot;email&quot;: &quot;keagan48@example.com&quot;
            },
            &quot;content&quot;: &quot;Ea dolorem beatae et rerum. Tempora aut ea omnis eos. Officia nemo consequatur eos et voluptate ab. Maxime aut facilis nihil sint fuga sit repellat ad.&quot;
        },
        {
            &quot;id&quot;: 12,
            &quot;user&quot;: {
                &quot;id&quot;: 24,
                &quot;email&quot;: &quot;jhills@example.net&quot;
            },
            &quot;content&quot;: &quot;Fugiat sapiente minima pariatur eum harum sequi. Odit eos debitis illum minus nulla vel et quibusdam. Praesentium autem quaerat nihil excepturi.&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-topics" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-topics"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-topics"></code></pre>
</span>
<span id="execution-error-GETapi-v1-topics" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-topics"></code></pre>
</span>
<form id="form-GETapi-v1-topics" data-method="GET"
      data-path="api/v1/topics"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-topics', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-topics"
                    onclick="tryItOut('GETapi-v1-topics');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-topics"
                    onclick="cancelTryOut('GETapi-v1-topics');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-topics" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/topics</code></b>
        </p>
                    </form>

            <h2 id="topic-POSTapi-v1-topics">Create</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-POSTapi-v1-topics">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://localhost/api/v1/topics" \
    --header "Authorization: Bearer {API TOKEN}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"content\": \"qocgppuqmxpsfrtqstsdodorscpddvrufvsujhelsqqsybkpszayvuomuctzmgwuzjjxxrsbjeidoxtfczrbvbrhxyhogfvuzocktfkzzvjkwvbhnsempxmkwkiyijpfeyothshuvoxzyayvzaehadaooinvjkenqisrjdnrzybwqpasmqkcrjvjlrekeivmbxojthnmhwptksczytyyeblwvsxxwktimxoencnjzsdepryssihgsiidpycvqpuxqqasvbrmcynkjxtnhebfhtuwismuiiyefygnjcxpqbumkihobbjbgknuuvhumavfdqicgcrkxjlwldagrkcmcumoedwxvmtflwarvudtkgidguodhufkeoyiwufkeowmflapoatcnluixiepyaeqjhvrexehjbnzmftykuecrcbjvnertqwogqzlwuhlgkkeltwtdfzijpphlxhqcpwnqxayapmozejzriwsrqiddtrendueojptideycnebjdoyflzbtqvdzgurjxcqzblrsgnnjpgboeavdoczgqxnxxmjebkstspsnu\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics"
);

const headers = {
    "Authorization": "Bearer {API TOKEN}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "content": "qocgppuqmxpsfrtqstsdodorscpddvrufvsujhelsqqsybkpszayvuomuctzmgwuzjjxxrsbjeidoxtfczrbvbrhxyhogfvuzocktfkzzvjkwvbhnsempxmkwkiyijpfeyothshuvoxzyayvzaehadaooinvjkenqisrjdnrzybwqpasmqkcrjvjlrekeivmbxojthnmhwptksczytyyeblwvsxxwktimxoencnjzsdepryssihgsiidpycvqpuxqqasvbrmcynkjxtnhebfhtuwismuiiyefygnjcxpqbumkihobbjbgknuuvhumavfdqicgcrkxjlwldagrkcmcumoedwxvmtflwarvudtkgidguodhufkeoyiwufkeowmflapoatcnluixiepyaeqjhvrexehjbnzmftykuecrcbjvnertqwogqzlwuhlgkkeltwtdfzijpphlxhqcpwnqxayapmozejzriwsrqiddtrendueojptideycnebjdoyflzbtqvdzgurjxcqzblrsgnnjpgboeavdoczgqxnxxmjebkstspsnu"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-topics">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 13,
        &quot;user&quot;: {
            &quot;id&quot;: 25,
            &quot;email&quot;: &quot;irma87@example.net&quot;
        },
        &quot;content&quot;: &quot;Consequatur voluptatem quia reiciendis ea voluptas fuga. Voluptas culpa porro aut sapiente reiciendis. Dolore excepturi molestiae non quasi ipsa ducimus aspernatur. Dolores sed quod ut.&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-topics" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-topics"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-topics"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-topics" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-topics"></code></pre>
</span>
<form id="form-POSTapi-v1-topics" data-method="POST"
      data-path="api/v1/topics"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {API TOKEN}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-topics', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-topics"
                    onclick="tryItOut('POSTapi-v1-topics');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-topics"
                    onclick="cancelTryOut('POSTapi-v1-topics');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-topics" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/topics</code></b>
        </p>
                <p>
            <label id="auth-POSTapi-v1-topics" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="POSTapi-v1-topics"
                                                                data-component="header"></label>
        </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>content</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="content"
               data-endpoint="POSTapi-v1-topics"
               value="qocgppuqmxpsfrtqstsdodorscpddvrufvsujhelsqqsybkpszayvuomuctzmgwuzjjxxrsbjeidoxtfczrbvbrhxyhogfvuzocktfkzzvjkwvbhnsempxmkwkiyijpfeyothshuvoxzyayvzaehadaooinvjkenqisrjdnrzybwqpasmqkcrjvjlrekeivmbxojthnmhwptksczytyyeblwvsxxwktimxoencnjzsdepryssihgsiidpycvqpuxqqasvbrmcynkjxtnhebfhtuwismuiiyefygnjcxpqbumkihobbjbgknuuvhumavfdqicgcrkxjlwldagrkcmcumoedwxvmtflwarvudtkgidguodhufkeoyiwufkeowmflapoatcnluixiepyaeqjhvrexehjbnzmftykuecrcbjvnertqwogqzlwuhlgkkeltwtdfzijpphlxhqcpwnqxayapmozejzriwsrqiddtrendueojptideycnebjdoyflzbtqvdzgurjxcqzblrsgnnjpgboeavdoczgqxnxxmjebkstspsnu"
               data-component="body" hidden>
    <br>
<p>Must be at least 3 characters. Must not be greater than 5000 characters.</p>
        </p>
        </form>

            <h2 id="topic-GETapi-v1-topics--id-">Get by id</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-topics--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://localhost/api/v1/topics/10" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics/10"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-topics--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 14,
        &quot;user&quot;: {
            &quot;id&quot;: 26,
            &quot;email&quot;: &quot;alexie14@example.org&quot;
        },
        &quot;content&quot;: &quot;Cum culpa voluptatem aspernatur quasi voluptatem et qui. Explicabo illum voluptatem reprehenderit numquam ipsam. Quibusdam consectetur omnis qui voluptate omnis.&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-topics--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-topics--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-topics--id-"></code></pre>
</span>
<span id="execution-error-GETapi-v1-topics--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-topics--id-"></code></pre>
</span>
<form id="form-GETapi-v1-topics--id-" data-method="GET"
      data-path="api/v1/topics/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-topics--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-topics--id-"
                    onclick="tryItOut('GETapi-v1-topics--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-topics--id-"
                    onclick="cancelTryOut('GETapi-v1-topics--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-topics--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/topics/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="GETapi-v1-topics--id-"
               value="10"
               data-component="url" hidden>
    <br>
<p>The ID of the topic.</p>
            </p>
                    </form>

            <h2 id="topic-PUTapi-v1-topics--id-">Update</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-PUTapi-v1-topics--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request PUT \
    "http://localhost/api/v1/topics/17" \
    --header "Authorization: Bearer {API TOKEN}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"content\": \"uqnsmzebiahdiemcwnhycgzwdeyuxbauwkzxstkezjfebzzppxzkehjfnvdpjxkugwbclvnukgbnigpjtvhunqtnnglunjxmywuatfrgifkzbbzgxlwwusxiplcunfkwgdcfuaashrldbanocmomtbiuviecqoilhevnoroufhbzvszgxnqdyapnkbhfoicjtcaqiaqphivntuudpqsxyzjurvxwrrfzonwpnrbqxtgjzeoebvxdwouspyzovmwnbijhnumhhhiawusgksnnksmwhtaxjztehejmjjacszeoywlpgwdybmzeihftsmndwtcdafmggqpptouiwhooqwlhgfuhocennwrgbocgzzhhzuldzglltpuwdhqetdqrlvfdijlmgscvdfpphwmvmplkwalhqvcgqocprolncwitwqfbpnaxfmtgxkxyedteaqqzzjicvzuthmvdnzuxobvayfbeojmnogclrezegiayczkcxzqmeyqpiqijgmlruqipjitgdzqysvwgdpzslvqjlvbtpjifuxihzjxpqllzoiswmgkefieycenktbewqppnkkvcpfzqhqmsxlcurbpcdqdkkckuziqdlgpkcpjaikwdftdkfowczurbbiasrogpcbwvchofacczuegvbrnkhjqdhbyoxukffguarwagnyozgolscwfdbuvgjwmrypttycbrhkmxjcrrsropxhybcdtxaivepnipbroqlvoxclqsogsaemxmyuekjcupnxlpxqgsgyhhvvsojeslvyerlnexyyzlgwfxrdpzmxvytmjhouytizvjjlhqsebmjqxkxaaakdhhdknbtdczuwvanmrnclmggzkkywpoecyharpribwapvhmegmuftovmhpsctmyuynvitmfyppxyaluihifcpgbkcsstgbzqddztksrfshycibfxvkrjniqxglxfhqlsruzvueieypqrfdbkfuyoeltghcugsbcagmqcfplwupdetertxtahrpztboatacmlewohijpbbdooeciljotakdzxvuwlgmabvbukdongjtpyqhuqtchyohrixztamlpakmdyhaidgtubesxwgiqlvajgbocvsgvfzzwihrlybnarzshujalyfexpozivkpeopfvfiygvdnnhzgbehlqsroskdlvxbkwyvjakwnwpavbvolmyelyrprjhbiorlrskvhzalsevigyyxulpkuzvfmznvfahkzwdccvgfmfuzedocwsaaunmvoumrwpsjwuaztryrwpcewucreftxdjfkrloiidcpufdewzuygbzbsevlonvmykxjbxfzgrlxmwcsbqzvabcenqlmddkvmbhadcplxrulilufaxfilxzkzkgjkrlqehkwsezxmmwohkshvmfpwcjninfklyphbcchjxkdohqsrbxlfhljayvdsizmnklpizalpvofrvjehlzdfyvkqpeufmxedxugcgnuzmrlizpfkylzogtxrefhjjearzvzikrntaybhrenncguajaxkjaxmlhraqzbgfymxfiksxlhkoooppwfpkvqixdokaxdjgoblmjiazmdgxfodpklqgsomdyvcuckowwgditdrrigxqicxjeixkrunkrzhocpkqsdkywwrzwdjvkjogazjyhegjoweckxofixvqxyanawukoxenyfswamsijzlptbnovwfrmqgglafhorssvbmzuepgahgdmdmjbusodtaybftjyyzldmcdkljpeugyjrqponjipciubzvhztxmxojhxjaafnymvzbkwkimcfivxpvbmmmsqlmygrhghfjmdfyuvwprzuduiphvsjmhkmonxeynwddwgmnyohglybfdsppewgpudnlpjjqksfsgiehhfvrarxrucqacqbeoqcfkjsnjwjntuowtkgdhuqngqclbbugwbjxvescsndilxqrehsdiltybtdmubbddeafbdofouxfcasbgugrsujvbwvaergtoytvrriogviixoggwxaedvsbxjvbkbwbadhoujfkwzhnxhztvclcymhitysjekzslugbeeibofrfechiodbzxxetoivcojwjgarwwrqksxtrtlxsxbapcdwbwytabasqeufpjjuowelljvdprpilfhgvicuqmlgybagisafgylzwtucncnekdnpdexkpnalgifcpewdlygjojqbvbwvqxmgzaoyciutcoolkpwlucwqmkbruujrqotemvqlxubbwpubmarfmiyyakbcgqxtuqihqypixyfcjvgqhrldzrsrqvribtzymfpirmjrhxjsrkoyysjzdhwnuzzjtbclpzlumntaqvazdbfdpoupichmnlyizntfvipyieyynxhlfupabnomganuhkpzklgikunzbavkmfbeifgoeeuswwiukmsmrdeabixrqjcysrppxusmsswhdbqfmqifkoxhaempazzqhrsmzmpemnbsszytlhtxdjzngcnfeoopugrxblvhwbjdbcpalbnurknuxekedidqaqenobpzekbvrrfiejczlpqxotvfjjabuizytsirojisxiaxmcgvinehtkmhivuhcrizwalevjtirfpvucpdsarlnomrsmdomeahslpwrvduccpjqmapgepimwvhldxczkcrdsvatodbbjiqdlfazxiakyowcjswfrusmhnoieoqzlktxtekbpwubdtubpxyayghmnhxvnvotjnersvnkxrmztycvimhemcymoxbiizuzfbgfyljczurdhwfewvavxnytlifupnxdijgebrfjjinjdhkmlamdstxpvjimwstgbvetiyndgwvmwqyismfdgcbwbynfjavertnkabjwzfrpbudklzgsbqrkecruixikdkfdbuqhvrgujksviturqoslrmpauqmtdfmqwytxdrotbsbsefclehmjwbjojlfkierwooayhjffotbzloilmvxpvdaaazzyjitgfihgezoupmcyxybbwgrmwqedabdfpmbmgpugoyazwzbrlyxpdjpqvvizoxlhdtmleyvsrxoouhubdnspcggliopmnzrcksquzmizjdrghclmorfmajmtullxctwyhzhzgzuwvcpngvjzywyzswfrqhxivdypnmifloekcpxkwbtjrkrilcuaeqvabsjrmywktslprhbvtjaehkibkfpxxxxrnjaeppreunqvsrohyexanaezxixswavahrppcnoqfnouhxdglzrmeajthtgbamkoxpnycuaddmkadvnkfnzpuvarylbpvimxkulqfyabtjzwokintdltuukgdqlecmrvaiwmaiwttoschlcapxxthxtnudumktagiqgbwmikxalesoaoamakjybkeshhjwamjuakvvkjvueplllylebsgsjrlssdpgalcialuwlawluclpftuyyjlbewksgfgmamfthxgexaqndgxqgrslbjivfotgdjuyyefizfgwcxeubajqtxjalhlbdapxqzsjfcstseuckxoschuenckldfhjmuhuqyzllmippcihnjyfudjclclttadkqjcfruiopbzvwpooggbbsrhljaexjziuuemwbbjcjxmfvmrfuvzwdqxqktxfgpuoxcbxkkrpbynrcmfqncaeqdxvfhumurwzgbnnqpcamnzqyfnqbomhqxkhncfmqlqwkcfbsqfinkwtkvalcvxisvshmsuvdzbyxwbpccjxckuemuauqsaafeuwkyourucixmhawoyhahdnbjcxualalroitqstquohwclxrzeqceahmpljpvnepbxspkfzvibgrpnanqpqatjqbaxijhmjwoctqvnnpmdxnbfjebtpqxngdmeblwhztamurfnojdhrkcxbqyogeidawcuivecpfmiurunvpsswnkzygitsoqxvetpamjleulawoguoxmyjgnggmzxfsksrahrehesrgmjyelpzcgjwzzwgdntapvvezcwmuajtymsqlcaehhyknakybdmcmpzpqpaxmedmnskhdyqkujkslfyosgaqaocrzydgmrdtbkmokjtjbavpwsityiyuyivrfxgfeihvywmraxswfxpzatdnktaqmbktbcqndmwrnxywasmcbpbcukwdjvzvyyvtcggclpfcnkmljptsyptwnvmfcnbgsmknozmupqrychsneymvgruxikljwsqxabjuktrysaytqltpsecsqwugteikwfffkyzmttwducmhvgpsbvpmhzjvbbdktnprcyzcvxykdzgykikmgflrawbjrlynqlqmuonomejcojarejomprpzpttlenvzljetiakibdkaftliifrqguajogpyaysssgolubzkrtdtjdtoymqarlnertqrsygdbckgzizccjyfmsclliqwkbvtclpagzqaseauoqcnahefuddziswmsjtayfbhevidoqqkogawkvspmnmfyfkrwyjetfwnnyjonysomjyayodonaomrnbydwxppgkpibjzheawvprlxfsldklxgrnucnlbgsykduwsgrqxlasimdeaxsncokjqknewnoncuchlokoyypwnimamxiryjbvbltjdicbfsaosearsmpzxjjgrrtkxixjgbjqbnvsxpon\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics/17"
);

const headers = {
    "Authorization": "Bearer {API TOKEN}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "content": "uqnsmzebiahdiemcwnhycgzwdeyuxbauwkzxstkezjfebzzppxzkehjfnvdpjxkugwbclvnukgbnigpjtvhunqtnnglunjxmywuatfrgifkzbbzgxlwwusxiplcunfkwgdcfuaashrldbanocmomtbiuviecqoilhevnoroufhbzvszgxnqdyapnkbhfoicjtcaqiaqphivntuudpqsxyzjurvxwrrfzonwpnrbqxtgjzeoebvxdwouspyzovmwnbijhnumhhhiawusgksnnksmwhtaxjztehejmjjacszeoywlpgwdybmzeihftsmndwtcdafmggqpptouiwhooqwlhgfuhocennwrgbocgzzhhzuldzglltpuwdhqetdqrlvfdijlmgscvdfpphwmvmplkwalhqvcgqocprolncwitwqfbpnaxfmtgxkxyedteaqqzzjicvzuthmvdnzuxobvayfbeojmnogclrezegiayczkcxzqmeyqpiqijgmlruqipjitgdzqysvwgdpzslvqjlvbtpjifuxihzjxpqllzoiswmgkefieycenktbewqppnkkvcpfzqhqmsxlcurbpcdqdkkckuziqdlgpkcpjaikwdftdkfowczurbbiasrogpcbwvchofacczuegvbrnkhjqdhbyoxukffguarwagnyozgolscwfdbuvgjwmrypttycbrhkmxjcrrsropxhybcdtxaivepnipbroqlvoxclqsogsaemxmyuekjcupnxlpxqgsgyhhvvsojeslvyerlnexyyzlgwfxrdpzmxvytmjhouytizvjjlhqsebmjqxkxaaakdhhdknbtdczuwvanmrnclmggzkkywpoecyharpribwapvhmegmuftovmhpsctmyuynvitmfyppxyaluihifcpgbkcsstgbzqddztksrfshycibfxvkrjniqxglxfhqlsruzvueieypqrfdbkfuyoeltghcugsbcagmqcfplwupdetertxtahrpztboatacmlewohijpbbdooeciljotakdzxvuwlgmabvbukdongjtpyqhuqtchyohrixztamlpakmdyhaidgtubesxwgiqlvajgbocvsgvfzzwihrlybnarzshujalyfexpozivkpeopfvfiygvdnnhzgbehlqsroskdlvxbkwyvjakwnwpavbvolmyelyrprjhbiorlrskvhzalsevigyyxulpkuzvfmznvfahkzwdccvgfmfuzedocwsaaunmvoumrwpsjwuaztryrwpcewucreftxdjfkrloiidcpufdewzuygbzbsevlonvmykxjbxfzgrlxmwcsbqzvabcenqlmddkvmbhadcplxrulilufaxfilxzkzkgjkrlqehkwsezxmmwohkshvmfpwcjninfklyphbcchjxkdohqsrbxlfhljayvdsizmnklpizalpvofrvjehlzdfyvkqpeufmxedxugcgnuzmrlizpfkylzogtxrefhjjearzvzikrntaybhrenncguajaxkjaxmlhraqzbgfymxfiksxlhkoooppwfpkvqixdokaxdjgoblmjiazmdgxfodpklqgsomdyvcuckowwgditdrrigxqicxjeixkrunkrzhocpkqsdkywwrzwdjvkjogazjyhegjoweckxofixvqxyanawukoxenyfswamsijzlptbnovwfrmqgglafhorssvbmzuepgahgdmdmjbusodtaybftjyyzldmcdkljpeugyjrqponjipciubzvhztxmxojhxjaafnymvzbkwkimcfivxpvbmmmsqlmygrhghfjmdfyuvwprzuduiphvsjmhkmonxeynwddwgmnyohglybfdsppewgpudnlpjjqksfsgiehhfvrarxrucqacqbeoqcfkjsnjwjntuowtkgdhuqngqclbbugwbjxvescsndilxqrehsdiltybtdmubbddeafbdofouxfcasbgugrsujvbwvaergtoytvrriogviixoggwxaedvsbxjvbkbwbadhoujfkwzhnxhztvclcymhitysjekzslugbeeibofrfechiodbzxxetoivcojwjgarwwrqksxtrtlxsxbapcdwbwytabasqeufpjjuowelljvdprpilfhgvicuqmlgybagisafgylzwtucncnekdnpdexkpnalgifcpewdlygjojqbvbwvqxmgzaoyciutcoolkpwlucwqmkbruujrqotemvqlxubbwpubmarfmiyyakbcgqxtuqihqypixyfcjvgqhrldzrsrqvribtzymfpirmjrhxjsrkoyysjzdhwnuzzjtbclpzlumntaqvazdbfdpoupichmnlyizntfvipyieyynxhlfupabnomganuhkpzklgikunzbavkmfbeifgoeeuswwiukmsmrdeabixrqjcysrppxusmsswhdbqfmqifkoxhaempazzqhrsmzmpemnbsszytlhtxdjzngcnfeoopugrxblvhwbjdbcpalbnurknuxekedidqaqenobpzekbvrrfiejczlpqxotvfjjabuizytsirojisxiaxmcgvinehtkmhivuhcrizwalevjtirfpvucpdsarlnomrsmdomeahslpwrvduccpjqmapgepimwvhldxczkcrdsvatodbbjiqdlfazxiakyowcjswfrusmhnoieoqzlktxtekbpwubdtubpxyayghmnhxvnvotjnersvnkxrmztycvimhemcymoxbiizuzfbgfyljczurdhwfewvavxnytlifupnxdijgebrfjjinjdhkmlamdstxpvjimwstgbvetiyndgwvmwqyismfdgcbwbynfjavertnkabjwzfrpbudklzgsbqrkecruixikdkfdbuqhvrgujksviturqoslrmpauqmtdfmqwytxdrotbsbsefclehmjwbjojlfkierwooayhjffotbzloilmvxpvdaaazzyjitgfihgezoupmcyxybbwgrmwqedabdfpmbmgpugoyazwzbrlyxpdjpqvvizoxlhdtmleyvsrxoouhubdnspcggliopmnzrcksquzmizjdrghclmorfmajmtullxctwyhzhzgzuwvcpngvjzywyzswfrqhxivdypnmifloekcpxkwbtjrkrilcuaeqvabsjrmywktslprhbvtjaehkibkfpxxxxrnjaeppreunqvsrohyexanaezxixswavahrppcnoqfnouhxdglzrmeajthtgbamkoxpnycuaddmkadvnkfnzpuvarylbpvimxkulqfyabtjzwokintdltuukgdqlecmrvaiwmaiwttoschlcapxxthxtnudumktagiqgbwmikxalesoaoamakjybkeshhjwamjuakvvkjvueplllylebsgsjrlssdpgalcialuwlawluclpftuyyjlbewksgfgmamfthxgexaqndgxqgrslbjivfotgdjuyyefizfgwcxeubajqtxjalhlbdapxqzsjfcstseuckxoschuenckldfhjmuhuqyzllmippcihnjyfudjclclttadkqjcfruiopbzvwpooggbbsrhljaexjziuuemwbbjcjxmfvmrfuvzwdqxqktxfgpuoxcbxkkrpbynrcmfqncaeqdxvfhumurwzgbnnqpcamnzqyfnqbomhqxkhncfmqlqwkcfbsqfinkwtkvalcvxisvshmsuvdzbyxwbpccjxckuemuauqsaafeuwkyourucixmhawoyhahdnbjcxualalroitqstquohwclxrzeqceahmpljpvnepbxspkfzvibgrpnanqpqatjqbaxijhmjwoctqvnnpmdxnbfjebtpqxngdmeblwhztamurfnojdhrkcxbqyogeidawcuivecpfmiurunvpsswnkzygitsoqxvetpamjleulawoguoxmyjgnggmzxfsksrahrehesrgmjyelpzcgjwzzwgdntapvvezcwmuajtymsqlcaehhyknakybdmcmpzpqpaxmedmnskhdyqkujkslfyosgaqaocrzydgmrdtbkmokjtjbavpwsityiyuyivrfxgfeihvywmraxswfxpzatdnktaqmbktbcqndmwrnxywasmcbpbcukwdjvzvyyvtcggclpfcnkmljptsyptwnvmfcnbgsmknozmupqrychsneymvgruxikljwsqxabjuktrysaytqltpsecsqwugteikwfffkyzmttwducmhvgpsbvpmhzjvbbdktnprcyzcvxykdzgykikmgflrawbjrlynqlqmuonomejcojarejomprpzpttlenvzljetiakibdkaftliifrqguajogpyaysssgolubzkrtdtjdtoymqarlnertqrsygdbckgzizccjyfmsclliqwkbvtclpagzqaseauoqcnahefuddziswmsjtayfbhevidoqqkogawkvspmnmfyfkrwyjetfwnnyjonysomjyayodonaomrnbydwxppgkpibjzheawvprlxfsldklxgrnucnlbgsykduwsgrqxlasimdeaxsncokjqknewnoncuchlokoyypwnimamxiryjbvbltjdicbfsaosearsmpzxjjgrrtkxixjgbjqbnvsxpon"
};

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-PUTapi-v1-topics--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 15,
        &quot;user&quot;: {
            &quot;id&quot;: 27,
            &quot;email&quot;: &quot;anais.sawayn@example.com&quot;
        },
        &quot;content&quot;: &quot;Qui numquam eveniet exercitationem exercitationem doloremque. Voluptas voluptatem dolorem temporibus vero optio et facere.&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-PUTapi-v1-topics--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-PUTapi-v1-topics--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-v1-topics--id-"></code></pre>
</span>
<span id="execution-error-PUTapi-v1-topics--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-v1-topics--id-"></code></pre>
</span>
<form id="form-PUTapi-v1-topics--id-" data-method="PUT"
      data-path="api/v1/topics/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {API TOKEN}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('PUTapi-v1-topics--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-PUTapi-v1-topics--id-"
                    onclick="tryItOut('PUTapi-v1-topics--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-PUTapi-v1-topics--id-"
                    onclick="cancelTryOut('PUTapi-v1-topics--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-PUTapi-v1-topics--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-darkblue">PUT</small>
            <b><code>api/v1/topics/{id}</code></b>
        </p>
                <p>
            <label id="auth-PUTapi-v1-topics--id-" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="PUTapi-v1-topics--id-"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="PUTapi-v1-topics--id-"
               value="17"
               data-component="url" hidden>
    <br>
<p>The ID of the topic.</p>
            </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>content</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="content"
               data-endpoint="PUTapi-v1-topics--id-"
               value="uqnsmzebiahdiemcwnhycgzwdeyuxbauwkzxstkezjfebzzppxzkehjfnvdpjxkugwbclvnukgbnigpjtvhunqtnnglunjxmywuatfrgifkzbbzgxlwwusxiplcunfkwgdcfuaashrldbanocmomtbiuviecqoilhevnoroufhbzvszgxnqdyapnkbhfoicjtcaqiaqphivntuudpqsxyzjurvxwrrfzonwpnrbqxtgjzeoebvxdwouspyzovmwnbijhnumhhhiawusgksnnksmwhtaxjztehejmjjacszeoywlpgwdybmzeihftsmndwtcdafmggqpptouiwhooqwlhgfuhocennwrgbocgzzhhzuldzglltpuwdhqetdqrlvfdijlmgscvdfpphwmvmplkwalhqvcgqocprolncwitwqfbpnaxfmtgxkxyedteaqqzzjicvzuthmvdnzuxobvayfbeojmnogclrezegiayczkcxzqmeyqpiqijgmlruqipjitgdzqysvwgdpzslvqjlvbtpjifuxihzjxpqllzoiswmgkefieycenktbewqppnkkvcpfzqhqmsxlcurbpcdqdkkckuziqdlgpkcpjaikwdftdkfowczurbbiasrogpcbwvchofacczuegvbrnkhjqdhbyoxukffguarwagnyozgolscwfdbuvgjwmrypttycbrhkmxjcrrsropxhybcdtxaivepnipbroqlvoxclqsogsaemxmyuekjcupnxlpxqgsgyhhvvsojeslvyerlnexyyzlgwfxrdpzmxvytmjhouytizvjjlhqsebmjqxkxaaakdhhdknbtdczuwvanmrnclmggzkkywpoecyharpribwapvhmegmuftovmhpsctmyuynvitmfyppxyaluihifcpgbkcsstgbzqddztksrfshycibfxvkrjniqxglxfhqlsruzvueieypqrfdbkfuyoeltghcugsbcagmqcfplwupdetertxtahrpztboatacmlewohijpbbdooeciljotakdzxvuwlgmabvbukdongjtpyqhuqtchyohrixztamlpakmdyhaidgtubesxwgiqlvajgbocvsgvfzzwihrlybnarzshujalyfexpozivkpeopfvfiygvdnnhzgbehlqsroskdlvxbkwyvjakwnwpavbvolmyelyrprjhbiorlrskvhzalsevigyyxulpkuzvfmznvfahkzwdccvgfmfuzedocwsaaunmvoumrwpsjwuaztryrwpcewucreftxdjfkrloiidcpufdewzuygbzbsevlonvmykxjbxfzgrlxmwcsbqzvabcenqlmddkvmbhadcplxrulilufaxfilxzkzkgjkrlqehkwsezxmmwohkshvmfpwcjninfklyphbcchjxkdohqsrbxlfhljayvdsizmnklpizalpvofrvjehlzdfyvkqpeufmxedxugcgnuzmrlizpfkylzogtxrefhjjearzvzikrntaybhrenncguajaxkjaxmlhraqzbgfymxfiksxlhkoooppwfpkvqixdokaxdjgoblmjiazmdgxfodpklqgsomdyvcuckowwgditdrrigxqicxjeixkrunkrzhocpkqsdkywwrzwdjvkjogazjyhegjoweckxofixvqxyanawukoxenyfswamsijzlptbnovwfrmqgglafhorssvbmzuepgahgdmdmjbusodtaybftjyyzldmcdkljpeugyjrqponjipciubzvhztxmxojhxjaafnymvzbkwkimcfivxpvbmmmsqlmygrhghfjmdfyuvwprzuduiphvsjmhkmonxeynwddwgmnyohglybfdsppewgpudnlpjjqksfsgiehhfvrarxrucqacqbeoqcfkjsnjwjntuowtkgdhuqngqclbbugwbjxvescsndilxqrehsdiltybtdmubbddeafbdofouxfcasbgugrsujvbwvaergtoytvrriogviixoggwxaedvsbxjvbkbwbadhoujfkwzhnxhztvclcymhitysjekzslugbeeibofrfechiodbzxxetoivcojwjgarwwrqksxtrtlxsxbapcdwbwytabasqeufpjjuowelljvdprpilfhgvicuqmlgybagisafgylzwtucncnekdnpdexkpnalgifcpewdlygjojqbvbwvqxmgzaoyciutcoolkpwlucwqmkbruujrqotemvqlxubbwpubmarfmiyyakbcgqxtuqihqypixyfcjvgqhrldzrsrqvribtzymfpirmjrhxjsrkoyysjzdhwnuzzjtbclpzlumntaqvazdbfdpoupichmnlyizntfvipyieyynxhlfupabnomganuhkpzklgikunzbavkmfbeifgoeeuswwiukmsmrdeabixrqjcysrppxusmsswhdbqfmqifkoxhaempazzqhrsmzmpemnbsszytlhtxdjzngcnfeoopugrxblvhwbjdbcpalbnurknuxekedidqaqenobpzekbvrrfiejczlpqxotvfjjabuizytsirojisxiaxmcgvinehtkmhivuhcrizwalevjtirfpvucpdsarlnomrsmdomeahslpwrvduccpjqmapgepimwvhldxczkcrdsvatodbbjiqdlfazxiakyowcjswfrusmhnoieoqzlktxtekbpwubdtubpxyayghmnhxvnvotjnersvnkxrmztycvimhemcymoxbiizuzfbgfyljczurdhwfewvavxnytlifupnxdijgebrfjjinjdhkmlamdstxpvjimwstgbvetiyndgwvmwqyismfdgcbwbynfjavertnkabjwzfrpbudklzgsbqrkecruixikdkfdbuqhvrgujksviturqoslrmpauqmtdfmqwytxdrotbsbsefclehmjwbjojlfkierwooayhjffotbzloilmvxpvdaaazzyjitgfihgezoupmcyxybbwgrmwqedabdfpmbmgpugoyazwzbrlyxpdjpqvvizoxlhdtmleyvsrxoouhubdnspcggliopmnzrcksquzmizjdrghclmorfmajmtullxctwyhzhzgzuwvcpngvjzywyzswfrqhxivdypnmifloekcpxkwbtjrkrilcuaeqvabsjrmywktslprhbvtjaehkibkfpxxxxrnjaeppreunqvsrohyexanaezxixswavahrppcnoqfnouhxdglzrmeajthtgbamkoxpnycuaddmkadvnkfnzpuvarylbpvimxkulqfyabtjzwokintdltuukgdqlecmrvaiwmaiwttoschlcapxxthxtnudumktagiqgbwmikxalesoaoamakjybkeshhjwamjuakvvkjvueplllylebsgsjrlssdpgalcialuwlawluclpftuyyjlbewksgfgmamfthxgexaqndgxqgrslbjivfotgdjuyyefizfgwcxeubajqtxjalhlbdapxqzsjfcstseuckxoschuenckldfhjmuhuqyzllmippcihnjyfudjclclttadkqjcfruiopbzvwpooggbbsrhljaexjziuuemwbbjcjxmfvmrfuvzwdqxqktxfgpuoxcbxkkrpbynrcmfqncaeqdxvfhumurwzgbnnqpcamnzqyfnqbomhqxkhncfmqlqwkcfbsqfinkwtkvalcvxisvshmsuvdzbyxwbpccjxckuemuauqsaafeuwkyourucixmhawoyhahdnbjcxualalroitqstquohwclxrzeqceahmpljpvnepbxspkfzvibgrpnanqpqatjqbaxijhmjwoctqvnnpmdxnbfjebtpqxngdmeblwhztamurfnojdhrkcxbqyogeidawcuivecpfmiurunvpsswnkzygitsoqxvetpamjleulawoguoxmyjgnggmzxfsksrahrehesrgmjyelpzcgjwzzwgdntapvvezcwmuajtymsqlcaehhyknakybdmcmpzpqpaxmedmnskhdyqkujkslfyosgaqaocrzydgmrdtbkmokjtjbavpwsityiyuyivrfxgfeihvywmraxswfxpzatdnktaqmbktbcqndmwrnxywasmcbpbcukwdjvzvyyvtcggclpfcnkmljptsyptwnvmfcnbgsmknozmupqrychsneymvgruxikljwsqxabjuktrysaytqltpsecsqwugteikwfffkyzmttwducmhvgpsbvpmhzjvbbdktnprcyzcvxykdzgykikmgflrawbjrlynqlqmuonomejcojarejomprpzpttlenvzljetiakibdkaftliifrqguajogpyaysssgolubzkrtdtjdtoymqarlnertqrsygdbckgzizccjyfmsclliqwkbvtclpagzqaseauoqcnahefuddziswmsjtayfbhevidoqqkogawkvspmnmfyfkrwyjetfwnnyjonysomjyayodonaomrnbydwxppgkpibjzheawvprlxfsldklxgrnucnlbgsykduwsgrqxlasimdeaxsncokjqknewnoncuchlokoyypwnimamxiryjbvbltjdicbfsaosearsmpzxjjgrrtkxixjgbjqbnvsxpon"
               data-component="body" hidden>
    <br>
<p>Must be at least 3 characters. Must not be greater than 5000 characters.</p>
        </p>
        </form>

            <h2 id="topic-DELETEapi-v1-topics--id-">Delete</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-DELETEapi-v1-topics--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "http://localhost/api/v1/topics/13" \
    --header "Authorization: Bearer {API TOKEN}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics/13"
);

const headers = {
    "Authorization": "Bearer {API TOKEN}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-v1-topics--id-">
            <blockquote>
            <p>Example response (204):</p>
        </blockquote>
                <pre>
<code>[Empty response]</code>
 </pre>
    </span>
<span id="execution-results-DELETEapi-v1-topics--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-v1-topics--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-v1-topics--id-"></code></pre>
</span>
<span id="execution-error-DELETEapi-v1-topics--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-v1-topics--id-"></code></pre>
</span>
<form id="form-DELETEapi-v1-topics--id-" data-method="DELETE"
      data-path="api/v1/topics/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {API TOKEN}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-v1-topics--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-v1-topics--id-"
                    onclick="tryItOut('DELETEapi-v1-topics--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-v1-topics--id-"
                    onclick="cancelTryOut('DELETEapi-v1-topics--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-v1-topics--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/v1/topics/{id}</code></b>
        </p>
                <p>
            <label id="auth-DELETEapi-v1-topics--id-" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="DELETEapi-v1-topics--id-"
                                                                data-component="header"></label>
        </p>
                <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="DELETEapi-v1-topics--id-"
               value="13"
               data-component="url" hidden>
    <br>
<p>The ID of the topic.</p>
            </p>
                    </form>

            <h2 id="topic-GETapi-v1-topics--idTopic--comments">Display comments</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-topics--idTopic--comments">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://localhost/api/v1/topics/11/comments" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/v1/topics/11/comments"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-topics--idTopic--comments">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 11,
            &quot;topic&quot;: {
                &quot;id&quot;: 16,
                &quot;user&quot;: {
                    &quot;id&quot;: 29,
                    &quot;email&quot;: &quot;lweber@example.com&quot;
                },
                &quot;content&quot;: &quot;Qui perferendis sed enim eveniet. Ipsum est ex molestiae saepe voluptatibus quibusdam. Voluptatum dignissimos atque est sed.&quot;
            },
            &quot;user&quot;: {
                &quot;id&quot;: 28,
                &quot;email&quot;: &quot;spencer.lexi@example.org&quot;
            },
            &quot;content&quot;: &quot;Vero molestias neque aut rerum et et ut. Adipisci sapiente occaecati aut tempora vel. Sit corporis voluptas ratione minima delectus. Et fugit aut aspernatur rerum.&quot;
        },
        {
            &quot;id&quot;: 12,
            &quot;topic&quot;: {
                &quot;id&quot;: 17,
                &quot;user&quot;: {
                    &quot;id&quot;: 31,
                    &quot;email&quot;: &quot;ikessler@example.com&quot;
                },
                &quot;content&quot;: &quot;Veniam et itaque rerum ad ipsa. Modi sint explicabo libero architecto. Rerum unde sed modi minus impedit.&quot;
            },
            &quot;user&quot;: {
                &quot;id&quot;: 30,
                &quot;email&quot;: &quot;iernser@example.net&quot;
            },
            &quot;content&quot;: &quot;Voluptatum hic omnis incidunt nisi enim. Sed qui et saepe aut consectetur quia autem. Sapiente dignissimos voluptas veritatis sed aut velit qui qui. Alias magni et ut a maiores.&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-topics--idTopic--comments" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-topics--idTopic--comments"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-topics--idTopic--comments"></code></pre>
</span>
<span id="execution-error-GETapi-v1-topics--idTopic--comments" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-topics--idTopic--comments"></code></pre>
</span>
<form id="form-GETapi-v1-topics--idTopic--comments" data-method="GET"
      data-path="api/v1/topics/{idTopic}/comments"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-topics--idTopic--comments', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-topics--idTopic--comments"
                    onclick="tryItOut('GETapi-v1-topics--idTopic--comments');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-topics--idTopic--comments"
                    onclick="cancelTryOut('GETapi-v1-topics--idTopic--comments');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-topics--idTopic--comments" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/topics/{idTopic}/comments</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>idTopic</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="idTopic"
               data-endpoint="GETapi-v1-topics--idTopic--comments"
               value="11"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

    

        
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                                        <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                                        <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                            </div>
            </div>
</div>
</body>
</html>
