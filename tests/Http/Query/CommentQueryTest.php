<?php

namespace Tests\Http\Query;

use App\Http\Query\CommentQuery;
use App\Http\Query\CommentQueryInterface;
use App\Models\Comment;
use App\Models\Topic;
use App\Repositories\CommentRepository;
use App\Repositories\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommentQueryTest extends TestCase
{
    use ProphecyTrait;

    private CommentRepositoryInterface|ObjectProphecy $commentRepository;

    private CommentQueryInterface $commentQuery;

    protected function setUp(): void
    {
        $this->commentRepository = $this->prophesize(CommentRepository::class);
        $this->commentQuery = new CommentQuery($this->commentRepository->reveal());

        parent::setUp(); // TODO: Change the autogenerated stub
    }

    public function testGetAll(): void
    {
        $this->commentRepository->all()->willReturn(new Collection([]))->shouldBeCalledTimes(1);

        $result = $this->commentQuery->getAll();

        $this->assertIsIterable($result);
    }

    public function testGetById(): void
    {
        $this->commentRepository->find(1)->willReturn(new Comment(['content' => 'test']))->shouldBeCalledTimes(1);

        $result = $this->commentQuery->getById(1);

        $this->assertInstanceOf(Comment::class, $result);
        $this->assertEquals($result->content, 'test');
    }

    public function testGetByIdNotFound(): void
    {
        $this->commentRepository->find(1)->willReturn(null)->shouldBeCalledTimes(1);
        $this->expectException(NotFoundHttpException::class);

        $this->commentQuery->getById(1);
    }

    public function testGetByIdNotFoundInvalidString(): void
    {
        $this->commentRepository->find('no-id')->willReturn(null)->shouldBeCalledTimes(1);
        $this->expectException(NotFoundHttpException::class);

        $this->commentQuery->getById('no-id');
    }

    public function testGetByTopic(): void
    {
        /** @var Topic|ObjectProphecy $topic */
        $topic = $this->prophesize(Topic::class);
        /** @var HasMany|ObjectProphecy $hasManyRelation */
        $hasManyRelation = $this->prophesize(HasMany::class);
        $hasManyRelation->getResults()->willReturn(new Collection([
            new Comment(),
            new Comment(),
            new Comment(),
        ]));

        $topic->comments()->willReturn($hasManyRelation->reveal());

        $comments = $this->commentQuery->getByTopic($topic->reveal());

        $this->assertCount(3, $comments);
    }

}
