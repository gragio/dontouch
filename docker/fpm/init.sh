#!/usr/bin/env bash

setfacl -dR -m u:deploy:rwX ./storage/framework ./storage/app
setfacl  -R -m u:deploy:rwX ./storage/framework ./storage/app

exec php-fpm8.0 -y /etc/php-fpm.conf
